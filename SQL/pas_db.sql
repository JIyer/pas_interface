-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 09, 2014 at 10:38 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pas_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE IF NOT EXISTS `documents` (
  `doc_id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_name` varchar(100) NOT NULL,
  `doc_link` varchar(500) NOT NULL,
  PRIMARY KEY (`doc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`doc_id`, `doc_name`, `doc_link`) VALUES
(1, 'Frequently Asked Questions', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3521500/Frequently%20Asked%20Questions_Draft4.doc'),
(2, 'PAS 3.0.0 UserGuide', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511890/PAS%203.0.0%20UserGuide.doc'),
(3, 'PAS Quick Start Preparation Guide', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511888/PAS%20Quick%20Start%20Preparation%20Guide%20v2.docx'),
(4, 'PAS Tips for Integration Engineers', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511889/PAS%20Tips%20for%20Integration%20Engineers.xls'),
(5, 'Product Description Document', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511894/Product%20Description%20Document%20PAS%203%200%20Issue%201.pdf'),
(6, 'How To create PAS AR', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511893/SLA%20for%20PAS%20Support%20(How%20To%20create%20PAS%20AR)_V1.docx');

-- --------------------------------------------------------

--
-- Table structure for table `mapping_customer_ne`
--

CREATE TABLE IF NOT EXISTS `mapping_customer_ne` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `c_id` int(10) NOT NULL,
  `NE_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `c_id` (`c_id`),
  KEY `NE_id` (`NE_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `mapping_customer_ne`
--

INSERT INTO `mapping_customer_ne` (`id`, `c_id`, `NE_id`) VALUES
(1, 2, 1),
(2, 2, 2),
(3, 1, 3),
(4, 2, 3),
(5, 2, 4),
(6, 1, 5),
(7, 2, 5),
(8, 1, 6),
(9, 2, 7),
(10, 1, 8),
(11, 1, 9),
(12, 2, 9),
(13, 2, 10),
(14, 1, 11),
(15, 2, 11),
(16, 2, 12),
(17, 1, 13),
(18, 2, 13),
(19, 1, 14),
(20, 2, 14),
(21, 2, 15),
(22, 1, 16),
(23, 2, 16);

-- --------------------------------------------------------

--
-- Table structure for table `mapping_ne_communicator`
--

CREATE TABLE IF NOT EXISTS `mapping_ne_communicator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `NE_id` int(11) NOT NULL,
  `comm_id` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `NE_id` (`NE_id`),
  KEY `comm_id` (`comm_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `mapping_ne_communicator`
--

INSERT INTO `mapping_ne_communicator` (`id`, `NE_id`, `comm_id`) VALUES
(1, 1, 1),
(2, 3, 2),
(3, 3, 3),
(4, 3, 4),
(5, 3, 5);

-- --------------------------------------------------------

--
-- Table structure for table `mapping_ne_configurator`
--

CREATE TABLE IF NOT EXISTS `mapping_ne_configurator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `NE_id` int(11) NOT NULL,
  `conf_id` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `NE_id` (`NE_id`),
  KEY `conf_id` (`conf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `master_communicator`
--

CREATE TABLE IF NOT EXISTS `master_communicator` (
  `comm_id` int(5) NOT NULL AUTO_INCREMENT,
  `platform` varchar(100) NOT NULL,
  `NE_release` varchar(50) NOT NULL,
  `comm_ver` mediumint(9) NOT NULL,
  `comm_name` varchar(100) NOT NULL,
  `comm_name_link` varchar(500) NOT NULL,
  `comm_RN` varchar(100) NOT NULL,
  `comm_RN_link` varchar(500) NOT NULL,
  `cpc_name` varchar(100) NOT NULL,
  `cpc_name_link` varchar(500) NOT NULL,
  `cpc_RN` varchar(100) NOT NULL,
  `cpc_RN_link` varchar(500) NOT NULL,
  `c_id` int(10) NOT NULL,
  `release_date` varchar(50) NOT NULL,
  PRIMARY KEY (`comm_id`),
  KEY `c_id` (`c_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `master_communicator`
--

INSERT INTO `master_communicator` (`comm_id`, `platform`, `NE_release`, `comm_ver`, `comm_name`, `comm_name_link`, `comm_RN`, `comm_RN_link`, `cpc_name`, `cpc_name_link`, `cpc_RN`, `cpc_RN_link`, `c_id`, `release_date`) VALUES
(1, 'mCAS HP C7000 G6/G8', '8.1', 45306, 'Collect_ALU_8950AAA_R8-R9_PAS2.3_v45306', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3490631/Collect_ALU_8950AAA_R8-R9_PAS2.3_v45306.dll', 'ReleaseNotes_Collect_ALU_8950AAA_R8-R9_PAS2.3_v45306', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3490634/Release%20Notes-Collect_ALU_8950AAA_R8-R9_PAS2.3_v45306.pdf', 'ALULabOnly_CPC-SDM_8950AAA_CP_SA_CPC1.0_MFV39_V1', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3489065/ALULabOnly_CPC-SDM_8950AAA_CP_SA_CPC1.0_MFV39_V1.zip', 'ReleaseNotes_ALULabOnly_CPC-SDM_8950AAA_CP_SA_CPC1.0_MFV39_V1', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3489067/ReleaseNotes_VzWonly_CPC-SDM_8950AAA_CP_SA_CPC1.0_MFV39.docx', 2, '8/19/2014'),
(2, 'LCP HP C7000/LCP ATCA', 'R27-28/10.2', 45011, 'Collect_ALU_5420CTS_R27-R28_PAS2.4.2_v45011', 'http://docushare.web.alcatel-lucent.com/dsweb/View/Collection-1061687', 'ReleaseNotes_Collect_ALU_5420CTS_R27-R28_PAS2.4.2_v45011', 'http://docushare.web.alcatel-lucent.com/dsweb/View/Collection-1061687', 'ALULabOnly_CPC-VOLTE_5420CTS_CPC1.0_MFV95_V1', 'http://docushare.web.alcatel-lucent.com/dsweb/View/Collection-970758 ', 'ReleaseNotes_ALULabOnly_CPC-VOLTE_5420CTS_CPC1.0_MFV95_V1', 'http://docushare.web.alcatel-lucent.com/dsweb/View/Collection-970758 ', 2, '7/28/2034'),
(3, 'LCP HP C7000/LCP ATCA', 'R27-28/10.2', 45011, 'Collect_ALU_5420CTS_R27-R28_PAS2.4.2_v45011', 'http://docushare.web.alcatel-lucent.com/dsweb/View/Collection-1061687', 'ReleaseNotes_Collect_ALU_5420CTS_R27-R28_PAS2.4.2_v45011', 'http://docushare.web.alcatel-lucent.com/dsweb/View/Collection-1061687', 'ALULabOnly_CTS5420_USP-C_1406_MFV168_GSR28_V1', 'http://docushare.web.alcatel-lucent.com/dsweb/View/Collection-995872', 'ReleaseNotes_ALULabOnly_CTS5420_USP-C_1406_MFV168_GSR28_V1', 'http://docushare.web.alcatel-lucent.com/dsweb/View/Collection-995872', 1, '7/28/2034'),
(4, 'LCP HP C7000/LCP ATCA', 'R27-28/10.2', 41838, 'Collect_ALU_5420CTS_R27-R28_PAS2.3_v41838', 'http://docushare.web.alcatel-lucent.com/dsweb/View/Collection-995360', 'Release_Notes_Collect_ALU_5420CTS_R27-R28_PAS2.3_v41838', 'http://docushare.web.alcatel-lucent.com/dsweb/View/Collection-995360', 'ALULabOnly_CPC-VOLTE_5420CTS_CPC1.0_MFV24_V1', 'http://docushare.web.alcatel-lucent.com/dsweb/View/Collection-970758', 'Release_notes_ALULabOnly_CPC-VOLTE_5420CTS_CPC1.0_MFV24_V1', 'http://docushare.web.alcatel-lucent.com/dsweb/View/Collection-970758', 2, '2/26/14'),
(5, 'LCP HP C7000/LCP ATCA', 'R27-28/10.2', 45011, 'Collect_ALU_5420CTS_R27-R28_PAS2.4.2_v45011', 'http://docushare.web.alcatel-lucent.com/dsweb/View/Collection-1061687', 'ReleaseNotes_Collect_ALU_5420CTS_R27-R28_PAS2.4.2_v45011', 'http://docushare.web.alcatel-lucent.com/dsweb/View/Collection-1061687', 'cpc just testing', 'http://docushare.web.alcatel-lucent.com/dsweb/View/Collection-970758 ', 'cpc just testing release notes', 'http://docushare.web.alcatel-lucent.com/dsweb/View/Collection-970758 ', 2, '7/28/2034');

-- --------------------------------------------------------

--
-- Table structure for table `master_configurator`
--

CREATE TABLE IF NOT EXISTS `master_configurator` (
  `conf_id` int(5) NOT NULL AUTO_INCREMENT,
  `platform` varchar(100) NOT NULL,
  `NE_release` varchar(50) NOT NULL,
  `conf_ver` varchar(50) NOT NULL,
  `conf_name` varchar(100) NOT NULL,
  `conf_name_link` varchar(500) NOT NULL,
  `conf_RN` varchar(100) NOT NULL,
  `conf_RN_link` varchar(500) NOT NULL,
  `cpc_name` varchar(100) NOT NULL,
  `cpc_name_link` varchar(500) NOT NULL,
  `cpc_RN` varchar(100) NOT NULL,
  `cpc_RN_link` varchar(500) NOT NULL,
  `c_id` int(10) NOT NULL,
  `release_date` varchar(50) NOT NULL,
  PRIMARY KEY (`conf_id`),
  KEY `c_id` (`c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `master_customer`
--

CREATE TABLE IF NOT EXISTS `master_customer` (
  `c_id` int(10) NOT NULL AUTO_INCREMENT,
  `customer` varchar(20) NOT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `master_customer`
--

INSERT INTO `master_customer` (`c_id`, `customer`) VALUES
(1, 'AT&T'),
(2, 'Verizon');

-- --------------------------------------------------------

--
-- Table structure for table `master_ne`
--

CREATE TABLE IF NOT EXISTS `master_ne` (
  `NE_id` int(11) NOT NULL AUTO_INCREMENT,
  `NE_name` varchar(20) NOT NULL,
  PRIMARY KEY (`NE_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `master_ne`
--

INSERT INTO `master_ne` (`NE_id`, `NE_name`) VALUES
(1, 'AAA'),
(2, 'COM'),
(3, 'CTS'),
(4, 'D-RTR'),
(5, 'IeCCF'),
(6, 'ISCSBC'),
(7, 'MGC-8'),
(8, 'MGW/BGW'),
(9, 'MRF'),
(10, 'MRF-PRBT'),
(11, 'PS/XDMS'),
(12, 'RTR'),
(13, 'SCG'),
(14, 'SDM-BE'),
(15, 'SDM-EXPERT'),
(16, 'SDM-FE');

-- --------------------------------------------------------

--
-- Table structure for table `master_setup`
--

CREATE TABLE IF NOT EXISTS `master_setup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setup` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `master_setup`
--

INSERT INTO `master_setup` (`id`, `setup`) VALUES
(1, 'Laptop'),
(2, 'Standalone'),
(3, 'COM_VM'),
(4, 'Upgrade');

-- --------------------------------------------------------

--
-- Table structure for table `master_version`
--

CREATE TABLE IF NOT EXISTS `master_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `versionNo` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `versionNo` (`versionNo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `master_version`
--

INSERT INTO `master_version` (`id`, `versionNo`) VALUES
(1, '2.3'),
(2, '2.3.1');

-- --------------------------------------------------------

--
-- Table structure for table `patches`
--

CREATE TABLE IF NOT EXISTS `patches` (
  `version_id` int(5) NOT NULL,
  `Release_Notes` varchar(500) NOT NULL,
  `Database_Patch` varchar(500) NOT NULL,
  `Timeout_Patch` varchar(500) NOT NULL,
  `Package_Patch` varchar(500) NOT NULL,
  `Webpas_Patch` varchar(500) NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patches`
--

INSERT INTO `patches` (`version_id`, `Release_Notes`, `Database_Patch`, `Timeout_Patch`, `Package_Patch`, `Webpas_Patch`) VALUES
(1, 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3419296/PAS%20R3.0.0%20Patches%20-%20Release%20Notes%20and%20Installation.doc', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511041/pas300_database_patch2.sql', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3506259/pas300_timeout_patch.sql', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3466018/pas300_package_patch_1.zip', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3488782/WebPAS.dll');

-- --------------------------------------------------------

--
-- Table structure for table `setup_1`
--

CREATE TABLE IF NOT EXISTS `setup_1` (
  `v_id` int(11) NOT NULL AUTO_INCREMENT,
  `Installation_Guide` varchar(150) NOT NULL,
  `User_Guide` varchar(150) NOT NULL,
  `Release_Notes` varchar(150) NOT NULL,
  `Upgrade_guide` varchar(150) DEFAULT NULL,
  `Load_exe_File` varchar(150) NOT NULL,
  `Load_ova_File` varchar(150) NOT NULL,
  `Webser_Comm_if` varchar(150) NOT NULL,
  `Quickstart_prep` varchar(150) DEFAULT NULL,
  `Checksum` varchar(150) NOT NULL,
  PRIMARY KEY (`v_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED AUTO_INCREMENT=3 ;

--
-- Dumping data for table `setup_1`
--

INSERT INTO `setup_1` (`v_id`, `Installation_Guide`, `User_Guide`, `Release_Notes`, `Upgrade_guide`, `Load_exe_File`, `Load_ova_File`, `Webser_Comm_if`, `Quickstart_prep`, `Checksum`) VALUES
(2, 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3072967/PAS%20Laptop%20VM%20Installation%20Guide%20v05.docx', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3072505/PAS%202.3.1%20UserGuide.doc', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3072519/Core%20Software%20Release%20Notes%20for%202-3-1.doc', ' ', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-2864692/VMware-player-5.0.0-812388.exe', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3073374/vmpas231.ova', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3072507/PAS%20Web%20Service%20Communication%20Interface_update5.docx', ' ', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3072526/Checksum_PAS2.3.1.txt'),
(1, 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3037344/PAS%20Laptop%20VM%20Installation%20Guide%20v04.docx', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3034123/PAS%202.3%20UserGuide.doc', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3034276/Core%20Software%20Release%20Notes%20for%202-3.doc', ' ', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-2864692/VMware-player-5.0.0-812388.exe', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3035490/vmpas23.ova', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3039892/PAS%20Web%20Service%20Communication%20Interface_update4.docx', ' ', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3039116/Checksum_PAS2.3.txt');

-- --------------------------------------------------------

--
-- Table structure for table `setup_2`
--

CREATE TABLE IF NOT EXISTS `setup_2` (
  `v_id` int(11) NOT NULL AUTO_INCREMENT,
  `Installation_Guide` varchar(150) NOT NULL,
  `User_Guide` varchar(150) NOT NULL,
  `Release_Notes` varchar(150) NOT NULL,
  `Upgrade_guide` varchar(150) DEFAULT NULL,
  `PA_Package_File` varchar(150) NOT NULL,
  `PAS_Database` varchar(150) NOT NULL,
  `Webser_Comm_if` varchar(150) NOT NULL,
  `Quickstart_prep` varchar(150) DEFAULT NULL,
  `Checksum` varchar(150) NOT NULL,
  PRIMARY KEY (`v_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED AUTO_INCREMENT=3 ;

--
-- Dumping data for table `setup_2`
--

INSERT INTO `setup_2` (`v_id`, `Installation_Guide`, `User_Guide`, `Release_Notes`, `Upgrade_guide`, `PA_Package_File`, `PAS_Database`, `Webser_Comm_if`, `Quickstart_prep`, `Checksum`) VALUES
(1, 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3039810/Parameter%20Audit%20SmartApp%20R2.3%20Installation%20and%20Configuration%20MOP.docx', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3034123/PAS%202.3%20UserGuide.doc', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3034276/Core%20Software%20Release%20Notes%20for%202-3.doc', ' ', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3039812/ParameterAudit_2.3.PackageFile', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3039906/pas_database.sql', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3039892/PAS%20Web%20Service%20Communication%20Interface_update4.docx', ' ', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3039116/Checksum_PAS2.3.txt'),
(2, 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3074103/Parameter%20Audit%20SmartApp%20R2.3.1%20Installation%20and%20Configuration%20MOP.do', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3072505/PAS%202.3.1%20UserGuide.doc', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3072519/Core%20Software%20Release%20Notes%20for%202-3-1.doc', ' ', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3071308/ParameterAudit_2.3.1.131030.PackageFile', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3074100/pas_database.sql', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3072507/PAS%20Web%20Service%20Communication%20Interface_update5.docx', ' ', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3072526/Checksum_PAS2.3.1.txt');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mapping_customer_ne`
--
ALTER TABLE `mapping_customer_ne`
  ADD CONSTRAINT `mapping_customer_ne_ibfk_1` FOREIGN KEY (`c_id`) REFERENCES `master_customer` (`c_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `mapping_customer_ne_ibfk_2` FOREIGN KEY (`NE_id`) REFERENCES `master_ne` (`NE_id`) ON DELETE CASCADE;

--
-- Constraints for table `mapping_ne_communicator`
--
ALTER TABLE `mapping_ne_communicator`
  ADD CONSTRAINT `mapping_ne_communicator_ibfk_1` FOREIGN KEY (`NE_id`) REFERENCES `master_ne` (`NE_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `mapping_ne_communicator_ibfk_2` FOREIGN KEY (`comm_id`) REFERENCES `master_communicator` (`comm_id`) ON DELETE CASCADE;

--
-- Constraints for table `mapping_ne_configurator`
--
ALTER TABLE `mapping_ne_configurator`
  ADD CONSTRAINT `mapping_ne_configurator_ibfk_1` FOREIGN KEY (`NE_id`) REFERENCES `master_ne` (`NE_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `mapping_ne_configurator_ibfk_2` FOREIGN KEY (`conf_id`) REFERENCES `master_configurator` (`conf_id`) ON DELETE CASCADE;

--
-- Constraints for table `master_communicator`
--
ALTER TABLE `master_communicator`
  ADD CONSTRAINT `master_communicator_ibfk_1` FOREIGN KEY (`c_id`) REFERENCES `master_customer` (`c_id`) ON DELETE CASCADE;

--
-- Constraints for table `master_configurator`
--
ALTER TABLE `master_configurator`
  ADD CONSTRAINT `master_configurator_ibfk_1` FOREIGN KEY (`c_id`) REFERENCES `master_customer` (`c_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
