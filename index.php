<?php
/*
 * Author : Janani Iyer
 *
 * This file acts as the frontend interface to the module
 *
 * @module_description 	This module is blah blah..
 * 
 */

// Comment below two lines to hide errors
ini_set("display_errors", "1");
error_reporting(E_ALL);
// ---


?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
<link rel="stylesheet" href="http://localhost/pas_interface/css/common.css">


</head>
<body>
	<div id="root-container">	<!-- root container begins -->
		<div id="header">
		
		</div>

		<div id="content"> 	 <!-- logic here -->
			<div class="heading">
			PAS WEBPAGE
			</div> 
			<div>
			<div id="doc">
	
					<ul id="menu">
	    
					    <li><a href="#">Starting with PAS</a>

					        <ul class="sub-menu">
					            <li>
					                <a href="http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511889/PAS%20Tips%20for%20Integration%20Engineers.xls">PAS tips for Integration Eng</a>
					            </li>
					            <li>
					                <a href="http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3521500/Frequently%20Asked%20Questions_Draft5.doc">FAQs</a>
					            </li>
					            <li>
					                <a href="http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511890/PAS%203.0.0%20UserGuide.doc">Latest PAS User Guide</a>
					            </li>
					            <li>
					                <a href="http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511894/Product%20Description%20Document%20PAS%203%200%20Issue%201.pdf">Product Description Document</a>
					            </li>
					            <li>
					                <a href="http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511888/PAS%20Quick%20Start%20Preparation%20Guide%20v2.docx">Quick Start Guide</a>
					            </li>
					        </ul>
					    </li>
					</ul>
				</div>
			</div> 
			<hr>

			<div id="communicators">
				<!-- <div id="doc">
	
					<ul id="menu">
	    
					    <li><a href="#">Starting with PAS</a>

					        <ul class="sub-menu">
					            <li>
					                <a href="http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511889/PAS%20Tips%20for%20Integration%20Engineers.xls">PAS tips for Integration Eng</a>
					            </li>
					            <li>
					                <a href="http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3521500/Frequently%20Asked%20Questions_Draft5.doc">FAQs</a>
					            </li>
					            <li>
					                <a href="http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511890/PAS%203.0.0%20UserGuide.doc">Latest PAS User Guide</a>
					            </li>
					            <li>
					                <a href="http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511894/Product%20Description%20Document%20PAS%203%200%20Issue%201.pdf">Product Description Document</a>
					            </li>
					            <li>
					                <a href="http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511888/PAS%20Quick%20Start%20Preparation%20Guide%20v2.docx">Quick Start Guide</a>
					            </li>
					        </ul>
					    </li>
					</ul>
				</div> -->
				<div class="selectors" id="div_pasbuilds">
					<div class="selectorGroupHeading">Download PAS Builds</div>

					<div>
						<label class="selectorsLabel" for="select_setup">Setup Type &raquo; </label>
						<select id="select_setup" onchange="fetch_version()"> <!-- <- on change remove links -->
							<option value="" selected>Select a Setup</option>
						</select>
					</div>

					<div>
						<label class="selectorsLabel" for="select_versionNo">PAS Version &raquo; </label>
						<select id="select_versionNo" onchange="fetch_setupinfo()">
							<option value="" selected>Select a Version</option>
						</select>
					</div>
					
				</div>



				<div class="selectors" id="div_pascommunicators">
					<div class="selectorGroupHeading">Download PAS Communicators/Configurators</div>

					<div>
					<label class="selectorsLabel" for="select_customer">Customer  &raquo; </label>
					<select id="select_customer" onchange="fetch_ne()">
						<option value="" selected>Select a Customer</option>
					</select>
					</div>

					<div>
					<label size="20px" class="selectorsLabel" for="select_networkElement">NE Type &nbsp &raquo; </label>
					<select id="select_networkElement" onchange="fetch_commconf()">
						<option value="" selected>Select a NE</option>
					</select>
					</div>

					<div>
						<div>
							<input type="radio" id = "radio_communicator" name="choice" value="communicator">
						</div>
						<div>
						<label class="selectorsLabel" for="select_communicator">Communicator &nbsp&nbsp&nbsp&nbsp&raquo; </label>
						<select id="select_communicator"  onchange="fetch_downloads(1)">
							<option value="" selected>Select a Version</option>
						</select>
						</div>
					</div>

					<div>
						<div>
							<input type="radio" id="radio_configurator" name="choice" value="configurator">
						</div>
						<div>
						<label class="selectorsLabel" for="select_configurator">Configurator &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&raquo; </label>
						<select id="select_configurator" onchange="fetch_downloads(2)">
							<option value="" selected>Select a Version</option>
						</select>
						</div>
					</div>
				</div>
			</div>


			<div id="info">
			<div>
				<div id="infoTableDiv">
				</div>
				<div id="patches">
				</div>
			</div> <!-- logic ends -->

		<div id="footer">
		</div>
	</div>	<!-- root container ends -->

<script src="http://localhost/COMMON-INCLUDES/jquery/jquery.js"></script>
<script type="text/javascript">
// console.log($.fn.jquery);

// GLOBAL VARIABLES
var DOMAIN         = "localhost";
var DOMAINADDR     = "http://"+DOMAIN+"/";
var MODULEFOLDER   = "pas_interface";
var MODULEPATH     = DOMAINADDR+MODULEFOLDER+"/";
var BACKKENDFOLDER = "backend";
var BACKENDPATH    = MODULEPATH+BACKKENDFOLDER+"/";

// FUNCTION DEFINATIONS HERE

function empty_selectBox(selectBoxID) {
	var initialVal = $(selectBoxID + " option[value='']").text();
	var optionString = "<option value=\"\" selected>"+initialVal+"</option>";
	$(selectBoxID).html(optionString);
}

function defaulting_select(selectBoxID) {
	$(selectBoxID).val('');
}

function is_null(optionValue) {
	if(optionValue == null || optionValue == 'undefined' || optionValue == "") 
		return 1;
	else 
		return 0;
}

function fetch_version() {
	// check if value is empty
	var valueOfSetup = $('#select_setup').find(':selected').attr('value');
	if (is_null(valueOfSetup)) {
		return;
	}

	$("#infoTableDiv").empty();
	$("#patches").empty();
	empty_selectBox("#select_networkElement");
	empty_selectBox("#select_communicator");
	empty_selectBox("#select_configurator");
	defaulting_select("#select_customer");

	//console.log(valueOfSetup);
	$.ajax({ 	// call backend and fetch data using ajax
		url: BACKENDPATH + "GET_Version.php",
		dataType : "JSON",

		success: function(response) {
			// console.log("---");
			// console.log(response);

			empty_selectBox("#select_versionNo");

			$.each(response, function(i, val) {
				var selOpt = document.createElement("option");
				selOpt.text = val['versionNo'];
				selOpt.value = val['id'];
				document.getElementById("select_versionNo").add(selOpt);
			});
		},
	});
}
//////////////////////

function patch_show(){
	
	var valueOfVersion = $('#select_versionNo').find(':selected').attr('value');

	if(is_null(valueOfVersion))
		return;
	
	$("#patches").empty();

	$.ajax ({
		url : BACKENDPATH + "GET_Patches.php?valueOfVersion="+valueOfVersion,
		dataType : "JSON",

		success: function(response) {
			// console.log(response);
			if(response == 0) {
				$("#patches").html("Sorry, No patches found.");

				return;
			}
			if(response == -1) {
				$("#patches").html("Sorry, your data could not be fetched.");
				return;
			}

			var rowString = "";
			rowString += "<div class='header_row' data-forfield='patches' data-celldisptype='regular'><b>PATCHES</b></div>";
				
			$.each(response, function(i, val) {
				rowString += "<div class='infoTableCell' data-celltype='"+i+"'><a class='download' href=" +val+ ">" +i+ "</a></div>";
			});

			$("#patches").css("display", "none").html(rowString).fadeIn(1000);
		}
	});
} 
/////////////////////

function fetch_setupinfo () {
	var valueOfVersion = $('#select_versionNo').find(':selected').attr('value');
	var valueOfSetup = $('#select_setup').find(':selected').attr('value');

	if (is_null(valueOfVersion) ||  is_null(valueOfSetup)) {
		return;
	}

	$.ajax({
		url: BACKENDPATH + "GET_Setup_Details.php?valueOfVersion="+valueOfVersion+"&valueOfSetup="+valueOfSetup,
		dataType: "JSON",

		success: function(response) {
			// console.log(response);
			if(response == 0) {
				$("#infoTableDiv").html("Sorry, No data found.");

				return;
			}
			if(response == -1) {
				$("#infoTableDiv").html("Sorry, your data could not be fetched.");
				return;
			}

			var rowString = "";
			rowString += "<div class='header_row' data-forfield='Builds' data-celldisptype='regular'><b>BUILDS</b></div>";
			$.each(response, function(i, val) {
				rowString += "<div class='infoTableCell' data-celltype='"+i+"'><a class='download' href=" +val+ ">" +i+ "</a></div>";
			});

			$("#infoTableDiv").css("display", "none").html(rowString).fadeIn(1000);
			patch_show();
		}
	});

}

function fetch_ne() {
	// check if value is empty
	var valueOfCustomer = $('#select_customer').find(':selected').attr('value');
	//console.log(valueOfCustomer);
	if (is_null(valueOfCustomer)) {
		return;
	}

	$("#infoTableDiv").empty();
	$("#patches").empty();
	//empty_selectBox("#select_setup");
	empty_selectBox("#select_versionNo");
	defaulting_select("#select_setup");

	$.ajax({ 	// call backend and fetch data using ajax
		url: BACKENDPATH + "GET_NE.php?valueOfCustomer="+valueOfCustomer,
		dataType : "JSON",

		success: function(response) {
			// console.log("---");
			//console.log(response);
			if(response == 0) {
				$("#infoTableDiv").html("Sorry, No data found.");

				return;
			}
			if(response == -1) {
				$("#infoTableDiv").html("Sorry, your data could not be fetched.");
				return;
			}
			
			empty_selectBox("#select_networkElement");

			$.each(response, function(i, val) {
				var selOpt = document.createElement("option");
				selOpt.text = val['NE_name'];
				selOpt.value = val['NE_id'];
				document.getElementById("select_networkElement").add(selOpt);
			});
		},
	});
}


function fetch_commconf () {
	var valueOfCustomer = $('#select_customer').find(':selected').attr('value');
	var valueOfNetworkElement = $('#select_networkElement').find(':selected').attr('value');
	//console.log(valueOfCustomer);
	//console.log(valueOfNetworkElement);
	if (is_null(valueOfCustomer) ||  is_null(valueOfNetworkElement)) {
		return;
	}

	$.ajax({
		url: BACKENDPATH + "GET_CommConf.php?valueOfCustomer="+valueOfCustomer+"&valueOfNetworkElement="+valueOfNetworkElement,
		dataType: "JSON",

		success: function(response) {
			var configurators = response['conf'];
			var communicators = response['comm'];

			if(response == -1) {
				$("#infoTableDiv").html("Sorry, your data could not be fetched.");
				return;
			}
			
			empty_selectBox("#select_communicator");
			empty_selectBox("#select_configurator");
			
			if(configurators == 0) {
				// do nothing
			}
			else {
				$.each(configurators, function(i, val) {
					var selOpt = document.createElement("option");
					selOpt.text = "v"+val['conf_ver'];
					selOpt.value = val['conf_ver'];
					document.getElementById("select_configurator").add(selOpt);
				});
			}

			if(communicators == 0) {
				// do nothing
			}
			else {
				$.each(communicators, function(i, val) {
					var selOpt = document.createElement("option");
					selOpt.text = "v"+val['comm_ver'];
					selOpt.value = val['comm_ver'];
					document.getElementById("select_communicator").add(selOpt);
				});
			}
		}
	});
}

function fetch_downloads(typeOfDownload) {
	if(typeOfDownload == 1) {	// for communicators
		var typeOfDownloadVer = $('#select_communicator').find(':selected').attr('value');
		var dTypeFull = "Communicator";
		var dTypeShort = "Comm";
	}
	if(typeOfDownload == 2) {	// for configurators
		var typeOfDownloadVer = $('#select_configurator').find(':selected').attr('value');
		var dTypeFull = "Configurator";
		var dTypeShort = "Conf";
	}

	var valueOfCustomer = $('#select_customer').find(':selected').attr('value');
	var valueOfNetworkElement = $('#select_networkElement').find(':selected').attr('value');

	$.ajax({
		url: BACKENDPATH+"GET_downloads.php",		
		data : {"valueOfCustomer" : valueOfCustomer, "valueOfNetworkElement" : valueOfNetworkElement, "typeOfDownloadVer" : typeOfDownloadVer, "typeOfDownload" : typeOfDownload},
		dataType :"JSON",

		error : function(jqXHR, textStatus, errorThrown) {
			console.log("Status :: " + textStatus);
			console.log("Error :: " + errorThrown);
			console.log(jqXHR);
			console.log("--------------------------------------");
			$('#infoTableDiv').html('Oops, Something went wrong! Please contact the dev for further debugging.');
		},

		success : function(response) {
			// do stuff 
			if(response == 0) {
				$("#infoTableDiv").html("Sorry, No data found.");
				return;
			}
			if(response == -1) {
				$("#infoTableDiv").html("Sorry, your data could not be fetched.");
				return;
			}
			
			$('#infoTableDiv').html('');

			htmlheader = "";
			htmlheader += "<div class='header_row' id='header_row' data-forDownloadType='"+typeOfDownload+"'>"; 
			htmlheader += "<div class='header_cell' data-cellDispType='regular' data-forField='platform'>Platform</div>";
			htmlheader += "<div class='header_cell' data-cellDispType='regular' data-forField='NE_release'>NE Release</div>";
			htmlheader += "<div class='header_cell' data-ispair = '1' data-cellDispType='link' data-forField='"+dTypeShort+"_name'>"+dTypeFull+" Name</div>";
			htmlheader += "<div class='header_cell' data-ispair = '1' data-cellDispType='link' data-forField='"+dTypeShort+"_RN'>"+dTypeFull+" Release Notes</div>";
			htmlheader += "<div class='header_cell' data-ispair = '1' data-cellDispType='link' data-forField='cpc_name'>CPC/GS Name</div>";
			htmlheader += "<div class='header_cell' data-ispair = '1' data-cellDispType='link' data-forField='cpc_RN'>CPC/GS Release Notes</div>";
			htmlheader += "<div class='header_cell' data-cellDispType='regular' data-forField='release_date'>Release Date</div>";
			htmlheader += "</div>";

			$('#infoTableDiv').append(htmlheader);
			
			var rowString = "";
			$.each(response, function(i, val) { 
				rowHTML = "<div class='data_row'>";
				$.each(val, function(fname,fvalue) {
					// var flag =1;
					// rowstring1 = "";
					// if(fname.search("_pair")>0) {
					// 	$.each(fvalue, function(name, link) {
														
					// 		if(flag%2==0){
					// 		rowString += "<div class='data_cell' data-cellType='"+name+"'><a href="+link+" target='_blank'>"+ rowstring1;
					// 		}
					// 		else{
					// 			rowstring1 += link+"</a> </div>";
					// 		}
					// 		flag++;
					// 	});
					// }
					// else {
					// 	rowString += "<div class='data_cell' data-cellType='"+fvalue+"'>"+ fvalue+" </div>";
					// }
					if(fname.indexOf("_pair") != -1) {		// find _pair in fname
						var pairIndex = [];
						var pairVals = [];
						$.each(fvalue, function(pIndex, iVal) {
							pairIndex.push(pIndex); 	// <- Just in case later
							pairVals.push(iVal);
						});
						rowHTML += "<div class='data_cell' data-cellDispType='link' data-cellType='"+fname+"' data-pairFor='"+pairIndex[0]+"'><a href="+pairVals[1]+" target=\"_blank\">"+pairVals[0]+"</a></div>";
					}
					else {		// no _pair in fname
						rowHTML += "<div class='data_cell' data-cellDispType='regular' data-cellType='"+fname+"'>"+fvalue+"</div>";
					}
				});
				rowHTML += "</div><br><br>";
				$('#infoTableDiv').append(rowHTML);
				//$("#infoTableDiv").css("display", "none").html(rowHTML).fadeIn(1000);
			});		// $.each ends
		},		// success function ends
	});
	
}

// DOCUMENT READY
$(document).ready(function() { 	/* documnet is ready now */
	$.ajax({ 	// call backend and fetch data using ajax
		url: BACKENDPATH + "GET_Initial.php",
		dataType : "json",

		success: function(response) {
			var setup = response['setup'];
			var customers = response['customers'];

			empty_selectBox("#select_setup");
			empty_selectBox("#select_customer"); // <- convert this function so that it accepts an array. array of any number of divs.

			// <- write a function that takes an array in the below format, and populates the selectboxes.
			// i.e. function create_selectboxOptions(arrayInThisFormat, selectboxId)
			$.each(setup, function(i, val) {		
				var valOfOption = val['id'];

				var selOpt = document.createElement("option");
				selOpt.text = val['setup'];
				selOpt.value = valOfOption;
				document.getElementById("select_setup").add(selOpt);
			});

			$.each(customers, function(i, val) {
				var valOfOption = val['c_id'];

				var selOpt = document.createElement("option");
				selOpt.text = val['customer'];
				selOpt.value = valOfOption;
				document.getElementById("select_customer").add(selOpt);
			});
		},
	});	// ajax ends

	$("#select_communicator, #radio_communicator").on('click', function() {
		$(this).parent().parent().removeClass("disabled");
		$("#radio_communicator").prop('checked',true);
		$("#select_configurator").parent().parent().addClass("disabled");

		//console.log($(this).parent().parent().find("input[name=choice]"));
	}); // experimentation ends

	$("#select_configurator, #radio_configurator").on('click', function() {
		$(this).parent().parent().removeClass("disabled");
		$("#radio_configurator").prop('checked',true);
		$("#select_communicator").parent().parent().addClass("disabled");

		//console.log($(this).parent().parent().find("input[name=choice]"));
	}); 
});		// document.ready ends

</script>


</body>
</html>