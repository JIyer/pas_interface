<?php
/*
 * Author : Janani Iyer
 *
 * This file will get all NE values particular to customer and return as JSON
 */

// Comment below two lines to hide errors
ini_set("display_errors", "1");
error_reporting(E_ALL);
// ---

if (!isset($_GET['valueOfCustomer']))
	exit;

$cId = $_GET["valueOfCustomer"];

require_once "vars/dbvars.php";

$connect = mysqli_connect($host, $username, $password);
if(mysqli_connect_errno()) {
	echo json_encode("-1");
	// echo mysqli_connect_error();
	mysqli_close($connect);
	exit;
}

$queryGetNE = "select b.NE_id, b.NE_name from pas_db.mapping_customer_ne a left join pas_db.master_ne b on a.NE_id = b.NE_id where a.c_id = $cId";
//echo ($queryGetNE);

if(!$result= mysqli_query($connect, $queryGetNE)) {
	echo json_encode(array("-1"));
	mysqli_close($connect);
	exit;
}

if(mysqli_num_rows($result)==0) {
	echo json_encode(array(0));
	mysqli_close($connect);
	exit;
}

$selectFields = array();
while($row = mysqli_fetch_assoc($result)) {
	// echo json_encode($row);
	array_push($selectFields, $row);
	}

mysqli_close($connect);
echo json_encode($selectFields);
exit;