<?php
/*
 * Author : Janani Iyer
 *
 * This file will get all versions and return as JSON
 */
 
// Comment below two lines to hide errors
ini_set("display_errors", "1");
error_reporting(E_ALL);
// ---

require_once "vars/dbvars.php";

$connect = mysqli_connect($host, $username, $password);
if(mysqli_connect_errno()) {
	echo mysqli_connect_error();
}
$queryFetchVersion = "select id, versionNo from `pas_db`.`master_version` where 1";
$result = mysqli_query($connect, $queryFetchVersion);

$returnVal = array();
while ($row = mysqli_fetch_assoc($result)) {
	array_push($returnVal, $row);
}

mysqli_close($connect);

echo json_encode($returnVal);