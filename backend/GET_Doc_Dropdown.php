<?php
/*
 * Author : Saikat De
 *
 * This file will get all list of docs for the document dropdown and return as JSON
 */

// Comment below two lines to hide errors
ini_set("display_errors", "1");
error_reporting(E_ALL);
// ---

require_once "vars/dbvars.php";

$connect = mysqli_connect($host, $username, $password);
if(mysqli_connect_errno()) {
	echo json_encode("-1");
	// echo mysqli_connect_error();
	mysqli_close($connect);
	exit;
}
$queryGetDoclist = "select doc_name, doc_link from pas_db.documents";

//echo ($queryGetNE);

if(!$result= mysqli_query($connect, $queryGetDoclist)) {
	echo json_encode(array("-1"));
	mysqli_close($connect);
	exit;
}

if(mysqli_num_rows($result)==0) {
	echo json_encode(array(0));
	mysqli_close($connect);
	exit;
}

$selectFields = array();
while($row = mysqli_fetch_assoc($result)) {
	// echo json_encode($row);
	array_push($selectFields, $row);
	}

mysqli_close($connect);
echo json_encode($selectFields);
exit;