<?php
// Comment below two lines to hide errors
ini_set("display_errors", "1");
error_reporting(E_ALL);
// ---

$customerId = $_GET['valueOfCustomer'];
$valueOfNetworkElement = $_GET['valueOfNetworkElement'];
$typeOfDownloadVer = $_GET['typeOfDownloadVer'];
$typeOfDownload = $_GET['typeOfDownload'];

if($typeOfDownload == 1) {
	$typeOfDownload = "communicator"; $typeOfDownloadShort = "comm";
}
else {
	$typeOfDownload = "configurator"; $typeOfDownloadShort = "conf";
}

require_once "vars/dbvars.php";

try {
	$conn = mysqli_connect($host, $username, $password, "pas_db");
	if(mysqli_connect_errno()) {
		throw new Exception(mysqli_connect_error(), 1);
	}

	$queryGetDownloads = "SELECT platform, NE_release, ".$typeOfDownloadShort."_name, ".$typeOfDownloadShort."_name_link, ".$typeOfDownloadShort."_RN, ".$typeOfDownloadShort."_RN_link, cpc_name, cpc_name_link, cpc_RN, cpc_RN_link, release_date FROM pas_db.master_$typeOfDownload a right join mapping_ne_$typeOfDownload b on a.".$typeOfDownloadShort."_id=b.".$typeOfDownloadShort."_id where c_id='$customerId' AND ".$typeOfDownloadShort."_ver = $typeOfDownloadVer AND NE_id='$valueOfNetworkElement'";

	if(!$result = mysqli_query($conn, $queryGetDownloads)) {
		throw new Exception(mysqli_error($conn), 2);
	}

	if(mysqli_num_rows($result)==0) {
		throw new Exception(0, 3);
	}
	$resultsArr = array();

	while($row = mysqli_fetch_assoc($result)) {
		$resultsArr[] = $row;
	}

	// $resultsArr["heading"]["platform"]="Platform";
	// $resultsArr["heading"]["NE_release"]="NE Release";
	// $resultsArr["heading"][$typeOfDownloadShort."_name"]=ucfirst($typeOfDownload)." Name";
	// $resultsArr["heading"][$typeOfDownloadShort."_name_link"]=ucfirst($typeOfDownload)." Name Link";
	// $resultsArr["heading"][$typeOfDownloadShort."_RN"]=ucfirst($typeOfDownload)." Release Notes";
	// $resultsArr["heading"][$typeOfDownloadShort."_RN_link"]=ucfirst($typeOfDownload)." Release Notes Link";
	// $resultsArr["heading"]["cpc_name"]="CPC Name";
	// $resultsArr["heading"]["cpc_name_link"]="CPC Link";
	// $resultsArr["heading"]["cpc_RN"]="CPC Release Notes ";
	// $resultsArr["heading"]["cpc_RN_link"]="CPC Release Notes Link";
	// $resultsArr["heading"]["release_date"]="Release Date";

//manipulate results arr here
	
	$sendArr = array();
	foreach($resultsArr as $rowNo => $row) {
		$sendArr[] = array(
			'platform' => $row['platform'],
			'NE_release' => $row['NE_release'],
			'comm_name_pair' => array('comm_name'=>$row['comm_name'], 'comm_name_link' => $row['comm_name_link']),
			'comm_RN_pair' => array('comm_RN'=>$row['comm_RN'], 'comm_RN_link' => $row['comm_RN_link']),
			'cpc_name_pair' => array('cpc_name'=>$row['cpc_name'], 'cpc_name_link' => $row['cpc_name_link']),
			'cpc_RN_pair' => array('cpc_RN'=>$row['cpc_RN'], 'cpc_RN_link' => $row['cpc_RN_link']),
			'release_date' => $row['release_date'],
		);

	}
	// print_r($sendArr);
	$sendValues = json_encode($sendArr);
	echo $sendValues;
	mysqli_close($conn);
}
catch(Exception $error) {
	if($error->getCode() == 1) {
		echo "Could not connect to DB :: ".$error->getMessage();
	}
	else {
		if($error->getCode() == 2) {
			echo "Query Error :: ".$error->getMessage();
		}
		if($error->getCode() == 3) {
			echo json_encode(array(0));
		}
		mysqli_close($conn);
	}
}

exit;