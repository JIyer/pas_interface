<?php
/*
 * Author : Janani Iyer
 *
 * This file will get all communicator and configurator names for particular customer and NE (passsed as GET valuein URL) and return as JSON
 */

// Comment below two lines to hide errors
ini_set("display_errors", "1");
error_reporting(E_ALL);
// ---

if (!isset($_GET['valueOfCustomer']) || !isset($_GET['valueOfNetworkElement']))
	exit;

$cId = $_GET["valueOfCustomer"];
$NEId = $_GET["valueOfNetworkElement"];

require_once "vars/dbvars.php";

$connect = mysqli_connect($host, $username, $password);
if(mysqli_connect_errno()) {
	echo json_encode("-1");
	// echo mysqli_connect_error();
	mysqli_close($connect);
	exit;
}

$queryGetCommunicator = "SELECT comm_ver from pas_db.master_communicator a right join pas_db.mapping_ne_communicator b on a.comm_id = b.comm_id where c_id=$cId and ne_id = $NEId GROUP BY comm_ver ORDER BY comm_ver DESC";
// echo $queryGetCommunicator; exit;
if(!$result= mysqli_query($connect, $queryGetCommunicator)) {
	echo json_encode(array("-1"));
	mysqli_close($connect);
	exit;
}

$selectFieldsComm = array();
if(mysqli_num_rows($result)==0) {
	array_push($selectFieldsComm, 0);
}
else {
	while($row = mysqli_fetch_assoc($result)) {
		//echo json_encode($row);
		array_push($selectFieldsComm, $row);
		// $selectFieldsComm[] = $row; // Means same as above
	}
}

unset($result);

$queryGetConfigurator = "SELECT a.conf_id, conf_ver from pas_db.master_configurator a right join pas_db.mapping_ne_configurator b on a.conf_id = b.conf_id where c_id=$cId and ne_id = $NEId";

if(!$result= mysqli_query($connect, $queryGetConfigurator)) {
	echo json_encode(array("-1"));
	mysqli_close($connect);
	exit;
}

$selectFieldsConf = array();
if(mysqli_num_rows($result)==0) {
	array_push($selectFieldsConf, 0);
}
else {
	while($row = mysqli_fetch_assoc($result)) {
		//echo json_encode($row);
		array_push($selectFieldsConf, $row);
	}
}


mysqli_close($connect);
echo json_encode(array("comm" => $selectFieldsComm, "conf" => $selectFieldsConf));
exit;